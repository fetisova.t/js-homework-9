/**
 * Created on 02.07.2019.
 */
let tabsSwitchers = document.querySelectorAll('.tabs-title');
tabsSwitchers.forEach(function(elem){
    elem.addEventListener('click', function () {
        this.classList.add('active');
        let sibling = this.previousElementSibling;
        while(sibling){
            sibling.classList.remove('active');
            sibling = sibling.previousElementSibling;
        }
        sibling = this.nextElementSibling;
        while(sibling){
            sibling.classList.remove('active');
            sibling = sibling.nextElementSibling;
        }

        const tabContentData = `data-text="${this.dataset.text}"`,
              tabContent = document.querySelector(`.tabs-content-item[${tabContentData}]`),
              tabsContainer = tabContent.parentElement,
              tabContentItems = tabsContainer.children;
        console.log(tabContentData);
        console.log(tabContent);

        for(let i = 0; i<tabContentItems.length;i++){
            tabContentItems[i].classList.remove('active');
        }
        tabContent.classList.add('active');

    })
});